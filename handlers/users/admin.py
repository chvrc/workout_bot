from aiogram import types
from aiogram.dispatcher.filters.builtin import Command
from aiogram.dispatcher.storage import FSMContext
from data.config import ADMINS
from keyboards.inline.keyboard import kb_accept, kb_reject
import time
from datetime import datetime

from loader import dp
from utils.db_api.database import get_workout_records


@dp.message_handler(Command('rec'), user_id=ADMINS)
async def cmd_admin(message: types.Message):
    workout, users = get_workout_records()
    time_start = workout[1]
    days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
    day_of_week = days[datetime.utcfromtimestamp(time_start).weekday()]
    msg = (f"Тренировка\n"
            f"📅 {datetime.utcfromtimestamp(time_start).strftime('%d-%m-%Y %H:%M')} <b>{day_of_week}</b>\n")
    if users:
        msg += "\nУчастники:"
        for user in users:
            msg += f"\n<a href=\"tg://user?id={user[0]}\">{user[1]} {user[2]}</a>"
    else:
        msg += "\nУчастников нет"
    await message.answer(msg)
    
