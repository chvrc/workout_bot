from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart
from aiogram.dispatcher.storage import FSMContext
from keyboards.inline.keyboard import kb_accept, kb_reject
from data.config import ADMINS
from datetime import datetime

from loader import dp
from utils.db_api.database import add_user, add_workout_record, del_workout_record, get_user, get_workout_records


@dp.callback_query_handler(lambda c: c.data.startswith("accept"))
async def get_accept(call: types.CallbackQuery, state: FSMContext):
    workout, users = get_workout_records()
    user_ids = []
    for i in users:
        user_ids.append(i[0])
    user_id = call.from_user.id
    user = get_user(user_id)
    workout_id = int(call.data[6:])
    if not user:
        first_name = call.from_user.first_name
        last_name = call.from_user.last_name
        username = call.from_user.username
        language_code = call.from_user.language_code
        user = add_user(user_id, first_name, last_name, username, language_code)
    if workout_id == workout[0]:
        if user_id not in user_ids:
            add_workout_record(user[0], workout_id)
            await dp.bot.answer_callback_query(callback_query_id=call.id,
                                text='Вы записались')
            days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
            time_start = workout[1]
            day_of_week = days[datetime.utcfromtimestamp(time_start).weekday()]
            msg = (f"Новая запись\n<a href=\"tg://user?id={user[1]}\">{user[2]} {user[3]}</a>\n"
                f"📅 {datetime.utcfromtimestamp(time_start).strftime('%d-%m-%Y %H:%M')} <b>{day_of_week}</b>")
            for admin in ADMINS:
                await dp.bot.send_message(int(admin), msg)
        else:
            await dp.bot.answer_callback_query(callback_query_id=call.id,
                                text='Вы уже записаны')
    else:
        await dp.bot.answer_callback_query(callback_query_id=call.id,
                                text='Тренировка уже прошла')
