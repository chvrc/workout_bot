from aiogram import executor
import asyncio

from loader import dp
import middlewares, filters, handlers
from utils.db_api.database import add_chat, create_db
from utils.notify_admins import on_startup_notify
from utils.scheduler.scheduler import scheduler
from utils.set_bot_commands import set_default_commands


async def on_startup(dispatcher):
    # Устанавливаем дефолтные команды
    await set_default_commands(dispatcher)
    create_db()
    add_chat(-1001508832093, "Simply_Padel")
    asyncio.create_task(scheduler())

    # Уведомляет про запуск
    await on_startup_notify(dispatcher)


if __name__ == '__main__':
    executor.start_polling(dp, on_startup=on_startup, skip_updates=True)
