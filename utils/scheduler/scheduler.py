import asyncio
import aioschedule
from aiogram.dispatcher.storage import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.types.message import ContentType
from aiogram import types
import time
from datetime import datetime
from loader import dp
from utils.db_api.database import add_workout, get_chats
from keyboards.inline.keyboard import kb_accept, kb_reject


async def schedule_tuesday():
    chats = get_chats()
    time_start = int(time.time()) + 60 * 60 * 49
    time_end = time_start + 60 * 60 * 2
    description = "Тренировка"
    workout = add_workout(time_start, time_end, description)
    workout_id = workout[0]
    days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
    day_of_week = days[datetime.utcfromtimestamp(time_start).weekday()]
    msg = (f"{description}\n"
            f"📅 {datetime.utcfromtimestamp(time_start).strftime('%d-%m-%Y')} <b>{day_of_week}</b>\n"
            f"🕐 {datetime.utcfromtimestamp(time_start).strftime('%H:%M')} - "
            f"{datetime.utcfromtimestamp(time_end).strftime('%H:%M')}\n\n"
            "Нажмите кнопку \"+\" под сообщением, чтобы записаться")
    kb = await kb_accept(workout_id)
    for chat in chats:
        await dp.bot.send_message(chat[1], msg, reply_markup=kb)


async def schedule_thursday():
    chats = get_chats()
    time_start = int(time.time()) + 60 * 60 * 41
    time_end = time_start + 60 * 60 * 2
    description = "Тренировка"
    workout = add_workout(time_start, time_end, description)
    workout_id = workout[0]
    days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
    day_of_week = days[datetime.utcfromtimestamp(time_start).weekday()]
    msg = (f"{description}\n"
            f"📅 {datetime.utcfromtimestamp(time_start).strftime('%d-%m-%Y')} <b>{day_of_week}</b>\n"
            f"🕐 {datetime.utcfromtimestamp(time_start).strftime('%H:%M')} - "
            f"{datetime.utcfromtimestamp(time_end).strftime('%H:%M')}\n\n"
            "Нажмите кнопку \"+\" под сообщением, чтобы записаться")
    kb = await kb_accept(workout_id)
    for chat in chats:
        await dp.bot.send_message(chat[1], msg, reply_markup=kb)


async def schedule_saturday():
    chats = get_chats()
    time_start = int(time.time()) + 60 * 60 * 81
    time_end = time_start + 60 * 60 * 2
    description = "Тренировка"
    workout = add_workout(time_start, time_end, description)
    workout_id = workout[0]
    days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
    day_of_week = days[datetime.utcfromtimestamp(time_start).weekday()]
    msg = (f"{description}\n"
            f"📅 {datetime.utcfromtimestamp(time_start).strftime('%d-%m-%Y')} <b>{day_of_week}</b>\n"
            f"🕐 {datetime.utcfromtimestamp(time_start).strftime('%H:%M')} - "
            f"{datetime.utcfromtimestamp(time_end).strftime('%H:%M')}\n\n"
            "Нажмите кнопку \"+\" под сообщением, чтобы записаться")
    kb = await kb_accept(workout_id)
    for chat in chats:
        await dp.bot.send_message(chat[1], msg, reply_markup=kb)


async def scheduler():
    aioschedule.every().tuesday.at("21:00").do(schedule_tuesday)
    aioschedule.every().thursday.at("21:00").do(schedule_thursday)
    aioschedule.every().saturday.at("13:00").do(schedule_saturday)
    aioschedule.every().friday.at("13:45").do(schedule_saturday)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)