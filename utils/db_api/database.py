import sqlite3


def create_db():
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS users(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER UNIQUE,
        first_name TEXT,
        last_name TEXT,
        username TEXT,
        language_code TEXT)
    """)
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS workout(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        time_start INTEGER,
        time_end INTEGER,
        description TEXT)
    """)
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS workout_records(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user INTEGER,
        workout INTEGER,
        FOREIGN KEY(user) REFERENCES users(id),
        FOREIGN KEY(workout) REFERENCES workout(id))
    """)
    cur.execute(
    """
        CREATE TABLE IF NOT EXISTS chats(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        chat_id INTEGER UNIQUE,
        title TEXT)
    """)
    conn.commit()


# ---------- Пользователи ----------
def add_user(user_id, first_name, last_name, username, language_code):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR REPLACE
                INTO users(user_id, first_name, last_name, username, language_code)
                VALUES(?, ?, ?, ?, ?)
                """, (user_id, first_name, last_name, username, language_code))
    cur.execute("""SELECT * from users
                    WHERE user_id = ?
                    """, (user_id,))
    res = cur.fetchone()
    conn.commit()
    return res


def get_user(user_id):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT * from users
                    WHERE user_id = ?
                    """, (user_id,))
    res = cur.fetchone()
    conn.commit()
    return res


# ---------- Чаты ----------
def add_chat(chat_id, title):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR REPLACE
                INTO chats(chat_id, title)
                VALUES(?, ?)
                """, (chat_id, title))
    conn.commit()


def get_chats():
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT * from chats;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


# ---------- Тренировки ----------
def add_workout(time_start, time_end, description):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE
                INTO workout(time_start, time_end, description)
                VALUES(?, ?, ?);
                """, (time_start, time_end, description))
    cur.execute("""SELECT *
                    FROM workout
                    ORDER BY id DESC
                    LIMIT 1
                """)
    res = cur.fetchone()
    conn.commit()
    return res


# ---------- Участники тренировок ----------
def add_workout_record(user_id, workout_id):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE
                INTO workout_records(user, workout)
                VALUES(?, ?);
                """, (user_id, workout_id))
    conn.commit()


def get_workout_records():
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""SELECT *
                    FROM workout
                    ORDER BY id DESC
                    LIMIT 1
                """)
    workout = cur.fetchone()
    workout_id = workout[0]
    cur.execute("""SELECT u.user_id, u.first_name, u.last_name
                    FROM workout_records r
                    JOIN workout w ON r.workout = w.id
                    JOIN users u ON r.user = u.id
                    WHERE w.id = ?
                    """, (workout_id, ))
    users = cur.fetchall()
    conn.commit()
    return workout, users


def del_workout_record(user_id, workout_id):
    conn = sqlite3.connect('appdb.sqlite3')
    cur = conn.cursor()
    cur.execute("""DELETE FROM workout_records
                    WHERE user = ? AND workout = ?
                    """, (user_id, workout_id))
    res = cur.fetchone()
    conn.commit()
    return res