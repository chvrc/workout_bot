from aiogram.types import (InlineKeyboardMarkup, InlineKeyboardButton, 
                            ReplyKeyboardMarkup, KeyboardButton)


async def kb_accept(workout_id):
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('➕', callback_data=f'accept{workout_id}')
    kb.add(bt1)
    return kb


async def kb_reject(workout_id):
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('Вы записаны', callback_data=f'reject{workout_id}')
    kb.add(bt1)
    return kb
